import { Grid } from "@material-ui/core";
import React, { Component } from "react";

import UserService from "../../services/user.service";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  componentDidMount() {
    UserService.getPublicContent().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
    return (
      <div>
        <header>
          <h3>{this.state.content}</h3>
        </header>
        <Grid container spacing={1}>
          
        </Grid>
      </div>
    );
  }
}