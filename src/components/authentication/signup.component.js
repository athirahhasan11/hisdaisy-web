import React, { useState} from "react";
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import AuthService from "../../services/auth.service";

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(-1),
        width: '25ch',
      },
      paddingLeft: '10px',
    },
   
  }));

export default function SignUp(){
    const classes = useStyles();
    const [value, setValues ] = useState({
        username:'' ,
        password:'',
        email:'',
        role:''
    });

    const handleChange = name => event => {
        if(name === "username" && event.target.value === "a" ){
            alert(`name ${name}`);
        }
		setValues({...value, [name]: event.target.value});
	}

    const submit = event => {
       AuthService.register(value.username,value.email,value.password);
    }
    
    return(
        <>
           <Grid container spacing={3}>
            <Grid item xs={12}>
            <TextField label="username" className={classes.root} onChange={handleChange('username')}    />
            </Grid>
            <Grid item >
            <TextField label="role" className={classes.root} onChange={handleChange('username')}  name="role" />
            </Grid>
            <Grid item>
            <TextField label="password" className={classes.root} onChange={handleChange('password')}  name="password" />
            </Grid>
            <Grid item>
            <TextField label="email" className={classes.root} onChange={handleChange('email')}  name="email" />
            </Grid>
            </Grid>
            <br/>
            <Button size="medium" onClick={submit}>
                 Submit
            </Button>
        </>
    )
}
