import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import AuthService from "../../services/auth.service";

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: null,
      userReady: false,
      currentUser: { username: "" },
      value:0
    };
  }

 handleChange = (event, newValue) => {
    this.setState({
      value:newValue,
    })
  }

  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();

    if (!currentUser) this.setState({ redirect: "/home" });
    this.setState({ currentUser: currentUser, userReady: true })
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }

    return (
      <div >
        {(this.state.userReady) ?
        <div>
        <Paper variant="outlined" elevation={0}>
        <Grid container spacing={1}>
          <Grid item xs={12} md={2}>
          <Avatar variant="square" >
            N
        </Avatar>
          
          </Grid>
        </Grid>
        </Paper>
      </div>: null}
      </div>
    );
  }
}