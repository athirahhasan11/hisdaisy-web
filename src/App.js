import React, { useState, useEffect } from "react";
import { Switch, Route, Link } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import { makeStyles } from "@material-ui/core/styles";
import AuthService from "./services/auth.service";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Login from "./components/authentication/login.component";
import Register from "./components/authentication/signup.component";
import Home from "./components/authentication/home.component";
import Profile from "./components/authentication/profile.component";
import BoardUser from "./components/authentication/board-user.component";
import BoardModerator from "./components/authentication/board-moderator.component";
import BoardAdmin from "./components/authentication/board-admin.component";
import BoardChart from "./components/board/home";

import clsx from "clsx";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: "flex",
  },
  title: {
    flexGrow: 1,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(7) + 1,
    },
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawerContainer: {
    overflowY: "auto",
    overflowX: "hidden"
  },
  buttonDrawer: {
    position: "fixed",
    bottom: 0,
    left: 0,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(4),
  },
}));

function App() {
  const classes = useStyles();
  const [showModeratorBoard, setShowModerator] = useState(false);
  const [showAdminBoard, setshowAdminBoard] = useState(false);
  const [currentUser, setCurrentUser] = useState(undefined);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const [mobileOpen, setMobileOpen] = useState(false);

  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user) {
      setShowModerator(user.roles.includes("ROLE_MODERATOR"));
      setCurrentUser(user);
      setshowAdminBoard(user.roles.includes("ROLE_ADMIN"));
    }
  }, [currentUser]);

  const logOut = () => {
    AuthService.logout();
    setCurrentUser(undefined);
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleDrawerOpen = () => {
    setMobileOpen(true);
  };

  const handleDrawerClose = () => {
    setMobileOpen(false);
  };

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar variant="dense">
          <Link to={"/"}>DAISY</Link>

          {showModeratorBoard && (
            <Link to={"/mod"} className="nav-link">
              <Button color="inherit"> Moderator Board </Button>
            </Link>
          )}

          {showAdminBoard && (
            <Link to={"/admin"} className="nav-link">
              <Button color="inherit"> Admin Board </Button>
            </Link>
          )}

          {currentUser ? (
            <>
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                style={{ marginLeft: "auto" }}
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={handleClose} component={Link} to="/profile">
                  {" "}
                  Profile
                </MenuItem>

                <MenuItem onClick={logOut} component={Link} to="/login">
                  Logout
                </MenuItem>
              </Menu>
            </>
          ) : (
            <div style={{ marginLeft: "auto" }}>
              <Button color="inherit" component={Link} to="/login">
                Login
              </Button>
            </div>
          )}
        </Toolbar>
      </AppBar>

      {currentUser ? (
        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: mobileOpen,
            [classes.drawerClose]: !mobileOpen,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: mobileOpen,
              [classes.drawerClose]: !mobileOpen,
            }),
          }}
        >
          <div style={{ marginTop: "2.5rem" }}></div>
          <div className={classes.drawerContainer}>
            <List>
              <ListItem button component={Link} to="/board">
                <ListItemIcon>
                  <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="Dashboard" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <MailIcon />
                </ListItemIcon>
                <ListItemText primary="Inventory" />
              </ListItem>
              <ListItem button component={Link} to="/register">
                <ListItemIcon>
                  <MailIcon />
                </ListItemIcon>
                <ListItemText primary="Signp" />
              </ListItem>
            </List>
            <Divider />
          </div>
          <div className={classes.buttonDrawer}>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, {
                [classes.hide]: mobileOpen,
              })}
            >
              <ChevronRightIcon />
            </IconButton>

            <IconButton
              color="inherit"
              onClick={handleDrawerClose}
              edge="start"
              className={clsx(classes.menuButton, {
                [classes.hide]: !mobileOpen,
              })}
            >
              <ChevronLeftIcon />
            </IconButton>
          </div>
        </Drawer>
      ) : (
        <div></div>
      )}
      <main className={classes.content}>
        <Toolbar />
        <Switch>
          <Route path="/board" component={BoardChart}></Route>
          <Route exact path={["/", "/home"]} component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/profile" component={Profile} />
          <Route path="/user" component={BoardUser} />
          <Route path="/mod" component={BoardModerator} />
          <Route path="/admin" component={BoardAdmin} />
        </Switch>
      </main>
    </div>
  );
}

export default App;
